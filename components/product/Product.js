import * as React from 'react';
import { StyleSheet, View, Image, Text, Pressable } from 'react-native';
import { getProduct} from '../../functions/products';

export default class ProductComponent extends React.Component{
    constructor(props){
        super(props);

        //inizializziamo lo stato del prodotto
        this.state={
            //per ora gli passo un oggetto vuoto, così con la chiamata gli dico di
            //popolarmi l'oggetto vuoto 
            product:{}
        }
    }

    async componentDidMount(){
        console.log(await getProduct( this.props.route.params.id ));
        //restituisce una promise
        this.setState({
            product: await getProduct( this.props.route.params.id )
        });
    }

    render(){
        return(
            <View style={style.row}>
                <View style={style.oneThird}>
                    <Image source={ {uri: this.state.product.image} } style={style.productImage} />
                </View>
                <View style={style.twoThird}>
                    <Pressable onPress={() => this.props.navigation.navigate('Product', {id: this.state.product.id})}>
                        <Text>{ this.state.product.title }</Text>
                    </Pressable>
                </View>
            </View>
        )
    }
}


const style = StyleSheet.create({
        productImage: {
            height: 100,
            width: 100
        },
        oneThird: {
            flex: 0.33
        },
        row: {
            flex: 1,
            padding: 20
        },
        twoThird: {
            flex: 0.66
        }
    });