import React from "react";
import { StyleSheet, Text, View, Dimensions, Pressable, Platform } from "react-native";
import MapView, { Marker } from 'react-native-maps';
import { checkGeolocationPermission } from "../../functions/loaction";
//mi importo tutti gli elementi esposrtati ( * ) da expoLocation e li metto
//in una refarance "Location"
import * as Location from 'expo-location';

export default class LocationComponent extends React.Component {
    //Inizializiamo lo stato 
    //ricordiamoci di invocare proprs e super props.
    constructor(props) {
        super(props);
        //mi creo una proprietàprivata per creare una refarance
        this.map = React.createRef();
        this.marker = React.createRef();

        this.state = {
            //se dobbiamo condividere le proprietà di un copmonente va messa nello state
            //lo state rappresenta lo stato di un componente
            isMapLocked: false, //se la mappa è bloccata mi si sposta solo il marker
            latitude: 0,
            longitude: 0,
            lastLat: null,
            lastLong: null,
            latitudeDelta:  0.5,
            longitudeDelta: 0.5,
        };
    }

    //con questa funzione controlliamo i permessi dell'utente:
    //la prima cosa che deve fare questo cpomponente è vedere se abbiamo
    //i permessi per la geolocalizzaione
    //visto che lavoriamo con una funzione asyncrona, possiamo si agganciarci con il .then
    //come nelle slide o con async e await
    async componentDidMount() {
        //nel try ci metti quello che tu vorresti fare nel metodo del then
        //nel ctach ci va la gestione dell'errore
        let hasPermission;
        try {
            hasPermission = await checkGeolocationPermission();
        } catch (e) {
            console.log(e);
        }
        //a dispetto della slide, qui non piramidaliziamo il codice
        //perciò passerò prima quello che nella slide è l'else
        //quindi se (if !hasPermiss) non ho permessi, esco dalla funzione
        if (!hasPermission) {
            console.log('uauuuuu')
            return;
        }
        //qui invece mi vado a richiamare quello che contiene i permessi di navigazione
        //watchPosition è un metodo di react che mi va a vedere la posizione
        //dentro gli passerò cosa deve "ascolatre" (perchè watch è sempre in ascolto , visto che "guarda")
        //navigator.geolocation.watchPosition( p => console.log(p));
        //
        //quello di prima si trasfoma in questo, perchè è stata aggiornata la documentazione
        //e quindi tutto ciò che avevamo prima è stato spostato dentro Location
        //Location vuole un oggetto di configurazione, perciò per ora gli passiamo 
        //delle parentesi vuote {} 
        //Location.watchPositionAsync( {} , p => console.log(p))
        //aggiorno lo stato con setState
        Location.watchPositionAsync({
            accuracy: 6,
            distanceInterval: 1
        }, p => {
            //per evitare il prblema creatomi dai metodi dalla lettura di map e marker
            //se non trova niente nel map o nel marker, esci
            if(!this.map.current || !this.marker.current){
                return;
            }
            //racchiudiamo le coordinate in una costante così da poter richiare
            //solamente la costante tutte le volte che abbiamo bisogno delle coordinate
                const center = {
                    latitude: p.coords.latitude,
                    longitude: p.coords.longitude
                };
                //se la mappa NON è bloccata, gli cambio le coordinate quando le ricevo dal gps 
            if (!this.state.isMapLocked){                
                //con questo aggiorno le coordinate
                //si prendere due oggetti con varie prorpietà
                this.map.current.animateCamera({
                    center
                    }, {
                     duration: 1000
                });           
            }

            if (Platform.OS === 'android'){
                 this.marker.current.animateMarkerToCoordinate(center, 200);
            }else {
                this.marker.current.coordinate = center;
            }
                this.setState(center);   
        });

    }
  

    render() {
        return (
            <View>
                <MapView
                    initialRegion={{
                        latitude: this.state.latitude,
                        latitudeDelta: 0.01,
                        longitude: this.state.longitude,
                        longitudeDelta: 0.01,
                    }}
                    ref={this.map}
                    style={styles.map}>
                    <Marker
                        coordinate={{
                            latitude: this.state.latitude,
                            longitude: this.state.longitude
                        }}
                        //come muovere il marker
                        ref={this.marker}
                    ></Marker>
                </MapView>
                <Text style={styles.textCoordinateLat}>Lat: {this.state.latitude} </Text>
                <Text>Long: {this.state.longitude}</Text>
                <Pressable style={styles.mapLockBtn} onPress={() => this.toggleMapLock()}> 
                {/* Scrivo un ternario che mi restituisce una stringa, se è bloccato esce "Unlock", altrimenti "Lock" */}
                    <Text style={styles.textPress}>{this.state.isMapLocked ? 'Unlock' : 'Lock'}</Text>
                </Pressable>
                {/* <Pressable style={styles.zoomIn} > 
                    <Text style={styles.textPress}> + </Text>
                </Pressable>
                <Pressable style={styles.zoomOut}> 
                    <Text style={styles.textPress}> - </Text>
                </Pressable> */}
            </View>
        );
    }
    
    toggleMapLock() {
        this.setState({
            //a questa chiave associerò il valore negato 
            //perchè il toggle inverte uno stato
            isMapLocked: !this.state.isMapLocked
        });
    };

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    map: {
        width: Dimensions.get('window').width,
        //   height: Dimensions.get('window').height <-- prende in automatico altezza e larghezza del telefono
        height: 600
    },
    mapLockBtn:{
        borderWidth:1,
        borderColor:'black',
        backgroundColor: '#6699FF',
        width:50,
        height:30,
        borderRadius:10,
        bottom:0,
        right:1,
        position:"absolute"
    },
    textPress:{
        color:'white',
        textAlign:'center',
        justifyContent:'center',
        top:2
    },
    textCoordinateLat:{
        backgroundColor:'#99CC99',
        width:200,
        borderTopRightRadius:10,
        borderBottomRightRadius:10,
        top:1
    },
    zoomIn: {
        borderWidth:1,
        borderColor:'black',
        backgroundColor: '#6699FF',
        width:50,
        height:30,
        borderRadius:10,
        top:1
    },
    zoomOut: {
        borderWidth:1,
        borderColor:'black',
        backgroundColor: '#CCCCFF',
        width:50,
        height:30,
        borderRadius:10,
        top:1
    }
});
