
import React from "react";
import { Pressable, Text, View } from "react-native";

export default class HomeComponent extends React.Component {
    constructor (props){
        super(props);
    }

    render(){
        return(
            <View>
                <Text> Benvenuto! </Text>
                <Pressable  onPress={ () => this.props.navigation.navigate('Products', {}) }>
                    <Text> Tutti i prodotti </Text>
                </Pressable>
            </View>
        )
    }
}