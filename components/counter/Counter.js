import { useState } from "react";
import { Pressable, Text,  View } from "react-native";

//gli passo proprs come parametro inizlae (ad esempio il property dinding)così nel costruttore posso sceglie
//da che numero far partire il mio componente
export function Counter(props){
    //UseState vuole come unico parametro vuole il valore iniziale
    //che vogliamo dare alla singola proprietà che farà parte dello stato
    //questa funzione restituisce un array, dove alla posizione 0 abbiamo la proprietà dello stato (state prop)
    //alla seconda posizione ci troviamo una funzione di aggiornamento (quindi all'indice uno)
    //Io per aggiornare la proprietà dello stato, devo invocare per forza la funzione.
    //Lo stato è in sola lettura, immutabile, non posso alterarlo accedendo alla prorpietà
    //perciò lo altero solo attraverso una funzione di aggiornamento.
    // const s = useState(0);
    //Uso l'array destructory per assegnare le due posizioni dell'array.
    const [cnt, updateFn] = useState(props.start || 0); // --> se voglio partire da  zero, metto solo 0
    //cnt sarà il contataore dalla posizione 0, perchè è la propr dello stato inizializzato 
    //al valore passato alla funzione useState
    //updateFn, sarà la funzione di aggiornamento, 
    //da invocare per aggiornare la prop della prima posizione

    const doTheMath = (isSum) => {
        //passiamo una funzione che determina il risultato che dovrà assumere questa prop
        //v sarebbe il valore attuale di cnt,
        // updateFn( v => v + 1);
        // quindi possiamo passargli anche il valore di cnt, però  passando cnt ,
        // il valore nella funzione diventerebbe il fantasma del valore più esterno
        // updateFn( cnt => cnt + 1);  
        //
        //operazione sottrazione e somma:
       // const doTheMath = (p) =>{
        // if (p === true){
        //     updateFn(cnt => cnt +1);
        // } else {
        //     updateFn(cnt => cnt -1);
        // }
        //
        //soluzione di andrea con il ternario:
        updateFn(cnt => isSum ? cnt + 1 : cnt -1);
    }
  
    return (
        <View>
            
            <Pressable  onPress={() => doTheMath(true)}>
                <Text>Add On</Text>
            </Pressable>
            <Text>{ cnt } </Text>
            <Pressable  onPress={() => doTheMath(false)}>
                <Text>Subtract One</Text>
            </Pressable>
        </View>
    )
}
