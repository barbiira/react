//ricorda i file dei componenti sono nominati in pascal case
import React from "react";
import { FlatList, Image, View, StyleSheet, Text, Pressable  } from "react-native";
import { getProducts } from "../../../../functions/products";


const style = StyleSheet.create({

    productImage:{
        height: 100,
        width: 100
    },
    oneThird:{
        flex: 0.33
    },
    row: {
        // flexDirection: 'row'
        flex: 1,
        padding:20
    },
    twoThird:{
        flex:0.66
    }
})

export default class ProductsComponet extends React.Component{
    #_keyExtractor(product){
        //mi ritorno l'id, che essendo però numerico, lo associo a una stringa.
        return product.id.toString();  
    }

    //E' la funzione ch eva a renderizzare l'elemento
    //({}) <-- andiamo ad estrarre delle proprietà dall'oggetto (destrutturazione)
    //Perciò in questa posizione , il parametro attuale è un oggetto, del quale a me interessa solo Item
    //in item troverò la singola occorrenza dentro quale sto iterando in quell'array.
    //guardati react-native-list-rendere-item
    //#_renderItem ({item}){
        //per evitare che lo scope del "this" cambi, creiamo una arrow function
   #_renderItem = ({item}) => {
        return(
            <View style={style.row}>
               <View style={style.oneThird}>
                   {/* le prime graffe sono per delimitare l'oggetto che gli devo passare
                   le seconde per l'oggetto 
                   uri = è il percorso https senza il protocollo ('https//fakestoreapi.com/img/61pHAEJ4NML._AC_UX679_.jpg')*/}
                    <Image source={ {uri: item.image} } style={style.productImage} />
               </View>
               <View style={style.twoThird}>
                <Pressable onPress={ () => this.props.navigation.navigate('Product', {id: item.id})}>
                <Text>{item.title}</Text>
                </Pressable>
                <Text>{item.price}</Text>
               </View>
            </View>
        )
    }

    //finchè il costruttore contiene solo l'invocazione a super, possiamo
    //rimuoverlo, perchè invocando il costruttore di ReactCopoment sono
    //già quelle props. (...)
    constructor(props){
        super(props);
        //definiamo un array di prodotti:
        this.state = {
            products: []
        };
    }

    //Nel costruttore inizializziamo delle propr ed è sincrono
    //mentre il componetDidMount sarebbe di fatto l'ngOnInit
    //questo componente invece è asicrono
    //Nel componente inizializiamo lo stato, qui invece facciamo partire la chiamata
    async componentDidMount() {
        console.log(await getProducts());
        //potrei usare la chiamta Fetch o Axios, che sarà quella che implementiamo qui
    this.setState({
           products: await getProducts()
       });
    }
    
    render(){
        return(
            <View>
                {/* //Flatlist ha 3 chiavi */}
                <FlatList 
                data= { this.state.products } 
                keyExtractor= { this.#_keyExtractor }
                renderItem= { this.#_renderItem }>
                </FlatList>

            </View>
        );
    }
}
