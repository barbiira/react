import React from "react";
import { Pressable, Text, View } from "react-native";

//esntendiamo la classe con le proprietà di React.Component
export default class CounterCl extends React.Component{
    //dentro di essa ovviamente troviamo il costruttore
    constructor(props){
        //dove gli passo le proprietà ricevute di React.Component
        super(props);

        //Inizializzo lo stato del componente
        this.state = {
            //dentro lo stato andiamo a mettere il contatore
            cnt: props.start || 0
        };
    }
    
    doTheMath(isSum){
        //setState fa parte della superclasse React.Copmonent, per settare lo stato
        //Prende in input un oggetto strutturato come segue:
        //-chiave della propr dello stato da aggiornare
        //Il numero di chiavi è arbitrario
        this.setState({
            //cnt è la chiave da aggiornare
            //eseguo come in counter un meotodo ternario.
            cnt: isSum ? this.state.cnt + 1 : this.state.cnt - 1
        });
    }

    //viene invocato quando il costruttore deve essere renderizzato
    //per fare un parallelismo èè il metodo return del costruttore
    render(){
        return (
            //dentro i return metto il mio wrapper (sintassi jsx)
           <View>
               <Text>{ this.state.cnt }</Text>
               <Pressable onPress={() => this.doTheMath (true) }>
                   <Text>Add One</Text>
               </Pressable>
               <Pressable onPress={() => this.doTheMath (false) }>
                   <Text>Sub One</Text>
               </Pressable>
           </View>
        );
    }
}