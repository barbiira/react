import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, Text, View, Pressable } from 'react-native';
import { Counter } from './components/counter/Counter';
import CounterCl from './components/counter/counter-class/CounterCl';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import LocationComponent from './components/location/Location';

import HomeComponent from './components/home/Home';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import ProductComponent from './components/product/Product';
import ProductsComponet from './components/counter/counter-class/products/Products'




const Stack = createNativeStackNavigator();
//E' una funzione costruttore , lo notiamo dalla iniziale maiuscola
   export default function App() {
    return (
     <NavigationContainer>
        <Stack.Navigator initialRouterName='Home'>
          <Stack.Screen name='Home' component={ HomeComponent }/>
          <Stack.Screen name='Product' component={ ProductComponent }/>
          <Stack.Screen name='Products' component={ ProductsComponet }/>
        </Stack.Navigator>
     </NavigationContainer>
  );
 }





//quello che c'era prima del navigation in app:

//Navigator
// const navigator = createStackNavigator({
//   //creiamo delle associazioni fra nome e componente.
//   //nome che voglio dare al percorso di quel componente
//   //attraverso la proprietà screen punterò a quel componente
//   Home: { screen: HomeComponent },
//   Lacation: { screen: LocationComponent }
// },{
//   //qui mettiamo ciò che vogliamo mostrare
//   initialRouteName: 'Home'
// })

//Il compomente App ce lo creerà questa funzione
// export default createAppContainer(navigator);


  //questa lezione fa un return di una sintassi particolare
  //che prende il nome di JSX.
  //Questi in particolare sono dei selettori JSX, che stanno richiamando dei
  //componenti di reactNative
  // export default function App() {
  //   <View style={styles.container}>
  //     {/* <Text>HELLO WORD!!</Text> */}
  //     <StatusBar style="auto" />
      {/* <Pressable backgroundColor="lightblue">
          <Text>I'm pressable!</Text>
      </Pressable>
      <Text>BOTTONE ROSA SOTTO!</Text>
      <Button title={'click me'} color="#f194ff"> </Button> */}
     {/* <Counter start={5} /> */}
     {/* <CounterCl /> */}
     {/* <ProductsComponet></ProductsComponet> */}
//      <LocationComponent></LocationComponent>
//     </View>
//è il sostitutivo di css
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   }
// });