module.exports = function(api) {
  api.cache(true);
  return {
    plugins:[
      [
        //la prima posizione è il nome del plugin
        //la seconda posizione è l'oggetto di configurazione
        '@babel/plugin-proposal-private-methods', {
          "loose": true,
          "shippedProposals": true
        }
      ]
    ],
    presets: [
      'babel-preset-expo',
  ],
  };
};
