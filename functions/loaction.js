//mi importo getAsync e  Location (FOREGROUND, app aperta davanti 
// mentre BAACKGROUND è quando la localizzazione ti serve in background)
// import { askAsync, getAsync, LOCATION_FOREGROUND} from "expo-permissions";
//mi importo tutti gli elementi di expo-location
import * as Location from 'expo-location';

export const checkGeolocationPermission = async() =>{
    //Riscriviamo tutto con una sintassi diversa, per risolvere l'errore dei
    //nde modules in giallo, datomi dal primo import
    //nella documentazione però non abbiamo trovato risposte, quindi siamo andati a 
    //vedere a mano cosa ci fosse nell'oggetto che restituva questo metodo (const boh)
    //così da capire che la chave granted era racchiusa in esso
    //
    const { granted } = await Location.requestForegroundPermissionsAsync();
    return granted;
    
    //serve per fare la richiesta e poi darci il risultato
    //const boh = await Location.requestForegroundPermissionsAsync();
    //console.log(boh);

    //////////////////////////////

    //getAsync ci a va controllare se abbiamo i permessi oppure no
    //askAsync non solo ti dice s e ci sono o no i permessi, ma lo fa in relazione 
    //alla richiesta all'utente se l'utente consente il permesso "sempre" ,
    // lui se lo ricorda. Altrimenti chiederà i permessi ogni volta
    // const { status } = await askAsync(LOCATION_FOREGROUND);
    // if(status !== 'granted'){
    //     return false
    // }
    // return true;
    //possiamo anche scriverlo così, con il ternario:
    // return status === 'granted';
    //perchè gli operatori di comparazione mi restituiscono comunque true o false
    //quindi ci risparmiamo una chiamata in più allo Stack del JavascripEngin
    //perchè Js è un linguaggio interpretato e quindi l'interprete ha un suo stack.
    
}