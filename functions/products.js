import axios from "axios";
import { Product } from "../models/products";
import { Rating } from "../models/rating";

//esportiamo delle funzioni legate al funzionamento dei prodotti 
export const getProducts = async () => {
   //.get (inizirizzo + oggetto di configurazione), come in HttpClient di Angular
   //essendo che anche axios lavora con le promise, rendiamo utto asincrono
  try{
    //il link mi restituisce già un array
    const products = await axios.get('https://fakestoreapi.com/products');
    // console.log(products);
    return products.data.map( p => new Product(
      p.id,
      p.title,
      p.price,
      p.description,
      p.category,
      p.image,
      new Rating (p.rating.rate, p.rating.count)
    )); 
   } catch(e){
     //usiamo il try and chatch per ....
     console.error(e);
     return [];  
   }
} 



export const getProduct = async (id) => {
 try{
   const resp = await axios.get(`https://fakestoreapi.com/products/${id}`);
   return new Product(
     resp.data.id,
     resp.data.title,
     resp.data.price,
     resp.data.description,
     resp.data.category,
     resp.data.image,
     new Rating (resp.data.rating.rate, resp.data.rating.count)
   ); 
  } catch(e){
    console.error(e);
    return [];  
  }
} 